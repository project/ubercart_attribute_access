Ubercart Attributes Access Control support.

This module allows the admin/user to control access to options.
Take, for example, the case of an enrolments system for a theological
college. Students may be enrolled at different levels (Cert IV,
Undergraduate, Graduate), have proficiency in different languages
(English, Chinese, Hebrew, Greek) and be enroled under different fee
structures (Australia / Overseas student). This module allows you to
set up attributes and options for each of these aspects, and then
limit the options a student sees when completing the enrolment form
(catalog) based on options you have enabled in their user profile.

Howto:

1. Download and install in modules directory as per the normal
   procedure.
2. Enable in the Modules admin screen (admin/build/modules).
3. Create attributes at admin/store/attributes. When editing an
   attribute, you will see a new 'Ubercart Product Attribute Access
   Settings' fieldset.
   The first checkbox is a system-wide toggle for this attribute.
   If disabled, all other settings will be ignored. Intended for
   setting things up without disturbing other users.
   The second tickbox determines how the module will behave when
   restrictions leave just one option available. In some cases, you
   might want to just hide the option. In others, you might want to
   show the single remaining item as a text field. This tickbox
   toggles this behaviour.
4. The form for individual product classes (accessible via
   admin/store/products/classes) has also been given a checkbox
   to control at a product class level whether access controls
   are applied. This tick box appears on the Attributes tab for the
   class, on the far right. It must also be enabled for access controls
   to be applied.
5. In the settings for each user, a new multi-select list will be found
   for each attribute. This allows you to select which options are
   available to this user. Note that the default is for no options to
   be selected, so if you enable access controls without doing anything
   in user accounts, noone will be able to choose any attributes.

If an attribute is required and no options are available, all other
attributes and options will be hidden and the select button disabled
and relabeled as 'Unavailable'. I intend to modify this to simply hide
the product.

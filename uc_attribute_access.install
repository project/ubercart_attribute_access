<?php

/**
 * @file
 * Installation file for Ubercart Quickbooks module.
 */

/**
 * Implementation of hook_schema().
 */
function uc_attribute_access_schema() {
  return array(
    'uc_attribute_access' => array(
      'description' => t('Ubercart Attribute Based Access Control.'),
      'fields' => array(
        'uid' => array(
          'description' => t('The Drupal User being granted permission'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'oid' => array(
          'description' => t('The Attribute Option being allowed'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
      ),
      'indexes' => array(
        'uid' => array('uid'),
        'oid' => array('oid'),
      ),
      'primary key' => array('uid', 'oid'),
    )
  );
}

/**
 * Implementation of hook_install().
 */
function uc_attribute_access_install()
{
  $ret = array();

  drupal_install_schema('uc_attribute_access');

  $access_field = array(
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => FALSE,
      );

  // Flags controlling access to particular options.
  foreach(array('uc_class_attributes', 'uc_attribute_options') as $table) {
    db_add_field($ret, $table, 'attribute_access', $access_field);
  }

  // Flags controlling access to attributes.
  $access_field['not null'] = TRUE;
  foreach(array('uc_attributes', 'uc_class_attributes') as $table) {
    db_add_field($ret, $table, 'enable_attribute_access', $access_field);
  }
  db_add_field($ret, 'uc_attributes', 'hide_if_one_option', $access_field);

  return $ret;
}

/**
 * Implementation of hook_uninstall().
 */
function uc_attribute_access_uninstall() {
  global $FieldList;

  $ret = array();
  foreach(array('uc_attributes', 'uc_class_attributes', 'uc_attribute_options', 'uc_class_attribute_options') as $table) {
    db_drop_field($ret, $table, 'attribute_access');
  }
  foreach(array('uc_attributes', 'uc_class_attributes') as $table) {
    db_drop_field($ret, $table, 'enable_attribute_access');
  }
  drupal_uninstall_schema('uc_attribute_access');
  return $ret;
}
